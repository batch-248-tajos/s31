const http = require('http')

const port = 3000

const server = http.createServer((request, response) => {
    if (request.url == '/login') {
        response.writeHead(200, {'Content-Type':'text/plain'})
        response.end('Welcome to Login Page!')
    } else {
        response.writeHead(404, {'Content-Type':'text/plain'})
        response.end('Page Not Available!')
    }
})

server.listen(port)
console.log(`Server is running at localhost:${port}`)